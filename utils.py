import os
import random
import sys
import errno
import shutil
import os.path as osp
import itertools
import torch

import new_ImageFolder

is_torchvision_installed = True
try:
    import torchvision
except:
    is_torchvision_installed = False

class BalancedBatchSampler(torch.utils.data.sampler.Sampler):
    def __init__(self, dataset, train_idx, labels=None):
        self.labels = labels
        self.dataset = dict()
        self.balanced_max = 0
        # Save all the indices for all the classes
        #for idx in range(0, len(dataset)):
        for idx in train_idx:
            label = self._get_label(dataset, idx)
            if label not in self.dataset:
                self.dataset[label] = list()
            self.dataset[label].append(idx)
            self.balanced_max = len(self.dataset[label]) if len(self.dataset[label]) > self.balanced_max else self.balanced_max

        # Oversample the classes with fewer elements than the max
        for label in self.dataset:
            while len(self.dataset[label]) < self.balanced_max:
                self.dataset[label].append(random.choice(self.dataset[label]))
        self.keys = list(self.dataset.keys())
        self.currentkey = 0
        self.indices = [-1] * len(self.keys)

    def __iter__(self):
        while self.indices[self.currentkey] < self.balanced_max - 1:
            self.indices[self.currentkey] += 1
            yield self.dataset[self.keys[self.currentkey]][self.indices[self.currentkey]]
            self.currentkey = (self.currentkey + 1) % len(self.keys)
        self.indices = [-1] * len(self.keys)

    def _get_label(self, dataset, idx, labels=None):
        if self.labels is not None:
            return self.labels[idx].item()
        else:
            # Trying guessing
            dataset_type = type(dataset)
            if is_torchvision_installed and dataset_type is torchvision.datasets.MNIST:
                return dataset.train_labels[idx].item()
            elif is_torchvision_installed and dataset_type is new_ImageFolder.ImageFolder:
                return dataset.imgs[idx][1]
            #elif dataset_type is Albumentations_loader.AlbuDataLoader:
                #return dataset[idx][1]
            else:
                raise Exception("You should pass the tensor of labels to the constructor as second argument")

    def __len__(self):
        return self.balanced_max * len(self.keys)

def mkdir_if_missing(directory):
    if not osp.exists(directory):
        try:
            os.makedirs(directory)
        except OSError as e:
            if e.errno != errno.EEXIST:
                raise

class AverageMeter(object):
    """Computes and stores the average and current value.
       
       Code imported from https://github.com/pytorch/examples/blob/master/imagenet/base_main.py#L247-L262
    """
    def __init__(self):
        self.reset()

    def reset(self):
        self.val = 0
        self.avg = 0
        self.sum = 0
        self.count = 0

    def update(self, val, n=1):
        self.val = val
        self.sum += val * n
        self.count += n
        self.avg = self.sum / self.count

def save_checkpoint(state, is_best, fpath='checkpoint.pth.tar'):
    mkdir_if_missing(osp.dirname(fpath))
    torch.save(state, fpath)
    if is_best:
        shutil.copy(fpath, osp.join(osp.dirname(fpath), 'best_model.pth.tar'))


class Logger(object):
    def __init__(self, fpath=None):
        self.file = None
        self.terminal = sys.stdout
        if fpath is not None:
            mkdir_if_missing(os.path.dirname(fpath))
            self.file = open(fpath, 'w')

    def write(self, msg):
        self.terminal.write(msg)
        if self.file is not None:
            self.file.write(msg)

    def flush(self):
        if self.file is not None:
            self.file.flush()
            os.fsync(self.file.fileno())

    def close(self):
        if self.file is not None:
            self.file.close()



def _one_pass(iters):
    for it in iters:
        try:
            yield next(it)
        except StopIteration:
            pass #of some of them are already exhausted then ignore it.

def zip_varlen(*iterables):
    iters = [iter(it) for it in iterables]
    while True: #broken when an empty tuple is given by _one_pass
        val = tuple(_one_pass(iters))
        if val:
            yield val
        else:
            break


def get_lr(optimizer):
    for param_group in optimizer.param_groups:
        return param_group['lr']


def adjust_learning_rate(optimizer, epoch, args):
    if epoch == 59:
        lr_model = args.lr * 0.1
        for param_group in optimizer.param_groups:
            param_group['lr'] = lr_model

    elif epoch == 119:
        lr_model = get_lr(optimizer) * 0.1
        for param_group in optimizer.param_groups:
            param_group['lr'] = lr_model

    elif epoch == 179:
        lr_model = get_lr(optimizer) * 0.1
        for param_group in optimizer.param_groups:
            param_group['lr'] = lr_model




from typing import Iterator, Optional, List, TypeVar, Generic, Sized

T_co = TypeVar('T_co', covariant=True)

class Sampler(Generic[T_co]):
    r"""Base class for all Samplers.

    Every Sampler subclass has to provide an :meth:`__iter__` method, providing a
    way to iterate over indices of dataset elements, and a :meth:`__len__` method
    that returns the length of the returned iterators.

    .. note:: The :meth:`__len__` method isn't strictly required by
              :class:`~torch.utils.data.DataLoader`, but is expected in any
              calculation involving the length of a :class:`~torch.utils.data.DataLoader`.
    """

    def __init__(self, data_source: Optional[Sized]) -> None:
        pass

    def __iter__(self) -> Iterator[T_co]:
        raise NotImplementedError

    # NOTE [ Lack of Default `__len__` in Python Abstract Base Classes ]
    #
    # Many times we have an abstract class representing a collection/iterable of
    # data, e.g., `torch.utils.data.Sampler`, with its subclasses optionally
    # implementing a `__len__` method. In such cases, we must make sure to not
    # provide a default implementation, because both straightforward default
    # implementations have their issues:
    #
    #   + `return NotImplemented`:
    #     Calling `len(subclass_instance)` raises:
    #       TypeError: 'NotImplementedType' object cannot be interpreted as an integer
    #
    #   + `raise NotImplementedError()`:
    #     This prevents triggering some fallback behavior. E.g., the built-in
    #     `list(X)` tries to call `len(X)` first, and executes a different code
    #     path if the method is not found or `NotImplemented` is returned, while
    #     raising an `NotImplementedError` will propagate and and make the call
    #     fail where it could have use `__iter__` to complete the call.
    #
    # Thus, the only two sensible things to do are
    #
    #   + **not** provide a default `__len__`.
    #
    #   + raise a `TypeError` instead, which is what Python uses when users call
    #     a method that is not defined on an object.
    #     (@ssnl verifies that this works on at least Python 3.7.)


class PartialBalancedBatchSampler(torch.utils.data.sampler.Sampler):
    r"""Samples elements randomly balanced per class for each batch until to achieve the batch-size.
        When it achieve the batch size, the lenght of others classes will be 0.
        If it is not possibile to take n balanced_classes from a class because the class has less elements, it takes
        all last elements.
        If it is not possibile to take n_balanced_classes because the batch-size is full, the remaining
        elements will be take the next batch.

        Args:
            dataset (Dataset): dataset to sample from
            balanced_classes (int): number of random samples in a generic class for each batch.
            num_classes (int): number of classes
    """

    def __init__(self, dataset, balanced_classes, num_classes):
        self.dataset = dict()
        self.d = dataset
        self.balanced_classes = balanced_classes
        self.num_cl_list = list(range(num_classes))
        self.train_idx = list(range(len(dataset)))


    def __iter__(self):
        for idx in self.train_idx:
            label = self._get_label(self.d, idx)
            if label not in self.dataset:
                self.dataset[label] = []
            self.dataset[label].append(idx)

        len_classes_dataset = [len(i) for i in self.dataset.values()]
        if any(self.balanced_classes > i for i in len_classes_dataset):
            raise Exception('Number of balanced classes should be less than ->' + str(min(len_classes_dataset)))
        rand_tensor = []
        while any(self.dataset.values()):
            random.shuffle(self.num_cl_list)
            for i in self.num_cl_list:
                stack_class = len(self.dataset[i])
                if self.balanced_classes < stack_class:
                    r_values = random.sample(self.dataset[i], k=self.balanced_classes)
                    rand_tensor.append(r_values)
                    for j in r_values:
                        self.dataset[i].remove(j)
                else:
                    r_values = random.sample(self.dataset[i], k=stack_class)
                    rand_tensor.append(r_values)
                    for j in r_values:
                        self.dataset[i].remove(j)

        flatten = list(itertools.chain(*rand_tensor))
        return iter(flatten)


    def _get_label(self, dataset, idx):
        return dataset.imgs[idx][1]

    # def _get_label(self, dataset, idx):
    #         dataset_type = type(dataset)
    #         if isinstance(dataset, torchvision.datasets.ImageFolder):
    #             return dataset.imgs[idx][1]
    #         elif dataset_type is Albumentations_loader.AlbuDataLoader:
    #             return dataset[idx][1]
    #         else:
    #             raise Exception("You should pass the tensor of labels to the constructor as second argument")
    #             exit()
