import json
import os


def get_cifar100_superclass(name):
    if name in 'beaver, dolphin, otter, seal, whale':
        return 'aquatic mammals'
    if name in 'aquarium_fish, flatfish, ray, shark, trout':
        return 'fish'
    if name in 'orchids, poppy, roses, sunflowers, tulips':
        return 'flowers'
    if name in 'bottles, bowls, cans, cups, plates':
        return 'food containers'
    if name in 'apples, mushrooms, oranges, pears, sweet_peppers':
        return 'fruit and vegetables'
    if name in 'clock, computer_keyboard, lamp, telephone, television':
        return 'household electrical devices'
    if name in 'bed, chair, couch, table, wardrobe':
        return 'household furniture'
    if name in 'bee, beetle, butterfly, caterpillar, cockroach':
        return 'insects'
    if name in 'bear, leopard, lion, tiger, wolf':
        return 'large carnivores'
    if name in 'bridge, castle, house, road, skyscraper':
        return 'large man-made'
    if name in 'cloud, forest, mountain, plain, sea':
        return 'large natural'
    if name in 'camel, cattle, chimpanzee, elephant, kangaroo':
        return 'omnivores and herbivores'
    if name in 'fox, porcupine, possum, raccoon, skunk':
        return 'medium mammals'
    if name in 'crab, lobster, snail, spider, worm':
        return 'non-insect'
    if name in 'baby, boy, girl, man, woman':
        return 'people'
    if name in 'crocodile, dinosaur, lizard, snake, turtle':
        return 'reptiles'
    if name in 'hamster, mouse, rabbit, shrew, squirrel':
        return 'small mammals'
    if name in 'maple_tree, oak_tree, palm_tree, pine_tree, willow_tree':
        return 'trees'
    if name in 'bicycle, bus, motorcycle, pickup_truck, train':
        return 'vehicles 1'
    if name in 'lawn_mower, rocket, streetcar, tank, tractor':
        return 'vehicles 2'
    else:
        print("Error get superclass", i)
        exit()



def get_cifar100_super_super_class(name):
    'food containers, , household electrical devices, household forniture, '
    ', large man-made, large natural'

    'Animals, Plants, People, Vehicles'



    if name in 'aquatic mammals':
        return ''
    if name in 'fish':
        return ''

    else:
        print("Error get superclass", i)
        exit()



path_train = '/home/super/tmp_datasets/cifar100/train/'
path_annotation_train = '/home/super/tmp_datasets/cifar100/'


folder_class = []
h_dict_idx = {}
idx = 0

for i in (os.listdir(path_train)):
    if not i.endswith(".tar"):
        folder_class.append(i)
        h_dict_idx[i] = idx
        idx += 1

h_dict = {}


def insert_into_dict(i, n_files):
    for file_name in n_files:
        h_dict.setdefault(file_name, [])
        h_dict[file_name].append(i)


from nltk.corpus import wordnet as wn

all_categories = []
all_categoriesidx = []
counter_idx = []
for idx, i in enumerate(folder_class):
    try:
        i_superclass=get_cifar100_superclass(i)
    except Exception:
        print("Error")
        exit()

    if i_superclass not in all_categories:
        all_categories.append(i_superclass)
        n_files = os.listdir(path_train + '/' + i)
        insert_into_dict(all_categories.index(i_superclass), n_files)
    else:
        # get position
        get_idx = all_categories.index(i_superclass)
        n_files = os.listdir(path_train + '/' + i)
        insert_into_dict(get_idx, n_files)


# #TODO save
with open(path_annotation_train+'/train_annotation.json', 'w') as fp:
    json.dump(h_dict, fp)


print('level ',1, 'Categories:',all_categories)
for i, item in enumerate(all_categories):
    print(i, item)


#Only for test set
path_test = '/home/super/tmp_datasets/cifar100/test/'
path_annotation_test = '/home/super/tmp_datasets/cifar100/'
folder_class = []
h_dict_idx = {}
idx = 0

for i in (os.listdir(path_test)):
    if not i.endswith(".tar"):
        folder_class.append(i)
        h_dict_idx[i] = idx
        idx += 1




for idx, i in enumerate(folder_class):
    try:
        i_superclass=get_cifar100_superclass(i)
    except Exception:
        exit()



    # get position
    get_idx = all_categories.index(i_superclass)
    n_files = os.listdir(path_test + '/' + i)
    insert_into_dict(get_idx, n_files)






with open(path_annotation_test+'/test_annotation.json', 'w') as fp:
    json.dump(h_dict, fp)

