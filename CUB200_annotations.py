import json
import os


path_dataset='/home/super/tmp_datasets/CUB_200_2011/'
path_train_hierarchy = '/home/super/tmp_datasets/CUB_200_2011/CUB_hierarchy/train_images_4_level_V1.txt'


folder_class = []
h_dict_idx = {}
idx = 0
#122 genera, 37 families, and 13 orders
#genera, families, order =[] ,[] ,[]

h_dict = {}


def insert_into_dict(i, image_name):
    #for file_name in n_files:
    h_dict.setdefault(image_name, [])
    h_dict[image_name].append(i)


f = open(path_train_hierarchy, "r")
for x in f:
  folder_name, image_name = x.split('/')[0], x.split('/')[1].split()[0]
  genera, families, order = int(x.split()[2]), int(x.split()[3]), int(x.split()[4])
  #print(folder_name, genera, families, order)
  #n_files = os.listdir(path_dataset + '/train/' + folder_name)
  insert_into_dict(genera, image_name)
f.close()



# # #TODO save
with open(path_dataset+'/train_annotation.json', 'w') as fp:
    json.dump(h_dict, fp)

h_dict = {}


#Only for test set
path_test_hierarchy = '/home/super/tmp_datasets/CUB_200_2011/CUB_hierarchy/test_images_4_level_V1.txt'

f = open(path_test_hierarchy, "r")
for x in f:
  folder_name, image_name = x.split('/')[0], x.split('/')[1].split()[0]
  genera, families, order = int(x.split()[2]), int(x.split()[3]), int(x.split()[4])
  insert_into_dict(genera, image_name)
f.close()






with open(path_dataset+'/test_annotation.json', 'w') as fp:
    json.dump(h_dict, fp)

