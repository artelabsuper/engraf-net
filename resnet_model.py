"""
https://github.com/weiaicunzai/pytorch-cifar100/blob/master/models/resnet.py
"""
import os
import sys

BASE_DIR = os.path.dirname(
    os.path.dirname(os.path.dirname(os.path.dirname(
        os.path.abspath(__file__)))))
sys.path.append(BASE_DIR)

import torch.nn.functional as F
import torch
import torch.nn as nn

__all__ = [
    'resnet18',
    'resnet34',
    'resnet50',
    'resnet101',
    'resnet152',
]


class BasicBlock(nn.Module):
    """Basic Block for resnet 18 and resnet 34
    """
    expansion = 1

    def __init__(self, in_channels, out_channels, stride=1):
        super(BasicBlock, self).__init__()

        self.residual_branch = nn.Sequential(
            nn.Conv2d(in_channels,
                      out_channels,
                      kernel_size=3,
                      stride=stride,
                      padding=1,
                      bias=False), nn.BatchNorm2d(out_channels),
            nn.ReLU(inplace=True),
            nn.Conv2d(out_channels,
                      out_channels * BasicBlock.expansion,
                      kernel_size=3,
                      padding=1,
                      bias=False),
            nn.BatchNorm2d(out_channels * BasicBlock.expansion))

        self.shortcut = nn.Sequential()

        if stride != 1 or in_channels != BasicBlock.expansion * out_channels:
            self.shortcut = nn.Sequential(
                nn.Conv2d(in_channels,
                          out_channels * BasicBlock.expansion,
                          kernel_size=1,
                          stride=stride,
                          bias=False),
                nn.BatchNorm2d(out_channels * BasicBlock.expansion))

    def forward(self, x):
        return nn.ReLU(inplace=True)(self.residual_branch(x) +
                                     self.shortcut(x))


class BottleNeck(nn.Module):
    """Residual block for resnet over 50 layers
    """
    expansion = 4

    def __init__(self, in_channels, out_channels, stride=1):
        super(BottleNeck, self).__init__()
        self.residual_branch = nn.Sequential(
            nn.Conv2d(in_channels, out_channels, kernel_size=1, bias=False),
            nn.BatchNorm2d(out_channels),
            nn.ReLU(inplace=True),
            nn.Conv2d(out_channels,
                      out_channels,
                      stride=stride,
                      kernel_size=3,
                      padding=1,
                      bias=False),
            nn.BatchNorm2d(out_channels),
            nn.ReLU(inplace=True),
            nn.Conv2d(out_channels,
                      out_channels * BottleNeck.expansion,
                      kernel_size=1,
                      bias=False),
            nn.BatchNorm2d(out_channels * BottleNeck.expansion),
        )

        self.shortcut = nn.Sequential()

        if stride != 1 or in_channels != out_channels * BottleNeck.expansion:
            self.shortcut = nn.Sequential(
                nn.Conv2d(in_channels,
                          out_channels * BottleNeck.expansion,
                          stride=stride,
                          kernel_size=1,
                          bias=False),
                nn.BatchNorm2d(out_channels * BottleNeck.expansion))

    def forward(self, x):
        return nn.ReLU(inplace=True)(self.residual_branch(x) +
                                     self.shortcut(x))


class ResNet(nn.Module):
    def __init__(self, block, layers, arch, graft, num_classes=100, inter_layer=False):
        super(ResNet, self).__init__()
        self.inter_layer = inter_layer
        self.in_channels_1 = 64
        self.in_channels_2 = 64
        self.in_channels_mixed = 64
        self.arch=arch
        self.graft=graft


        if self.arch == 'resnet18' or self.arch == 'resnet50' or self.arch == 'resnet101' or self.arch == 'resnet152':
            self.conv1_1branch = nn.Sequential(
                nn.Conv2d(3, 64, kernel_size=3, padding=1, bias=False),
                nn.BatchNorm2d(64), nn.ReLU(inplace=True))

            self.stage2_1branch = self._make_layer(block, 64, layers[0], 1, 'branch_1')
            self.stage3_1branch = self._make_layer(block, 128, layers[1], 2, 'branch_1')
            self.stage4_1branch = self._make_layer(block, 256, layers[2], 2, 'branch_1')
            self.stage5_1branch = self._make_layer(block, 512, layers[3], 2, 'branch_1')
            self.avg_pool = nn.AdaptiveAvgPool2d((1, 1))
            self.last_layer = nn.Linear(512 * block.expansion, num_classes)


        elif self.arch == 'LLC_resnet18' or self.arch == 'LLC_resnet50' or self.arch == 'LLC_resnet101' or self.arch == 'LLC_resnet152':
            #TODO two branch_1
            self.conv1_1branch = nn.Sequential(
                nn.Conv2d(3, 64, kernel_size=3, padding=1, bias=False),
                nn.BatchNorm2d(64), nn.ReLU(inplace=True))

            self.stage2_1branch = self._make_layer(block, 64, layers[0], 1, 'branch_1')
            self.stage3_1branch = self._make_layer(block, 128, layers[1], 2, 'branch_1')
            self.stage4_1branch = self._make_layer(block, 256, layers[2], 2, 'branch_1')
            self.stage5_1branch = self._make_layer(block, 512, layers[3], 2, 'branch_1')
            self.avg_pool = nn.AdaptiveAvgPool2d((1, 1))

            #TODO two branch_2
            self.conv1_2branch = nn.Sequential(
                nn.Conv2d(3, 64, kernel_size=3, padding=1, bias=False),
                nn.BatchNorm2d(64), nn.ReLU(inplace=True))

            self.stage2_2branch = self._make_layer(block, 64, layers[0], 1, 'branch_2')
            self.stage3_2branch  = self._make_layer(block, 128, layers[1], 2, 'branch_2')
            self.stage4_2branch   = self._make_layer(block, 256, layers[2], 2, 'branch_2')
            self.stage5_2branch   = self._make_layer(block, 512, layers[3], 2, 'branch_2')

            # TODO two branch_mixed
            self.conv1_branch_mixed= nn.Sequential(
                nn.Conv2d(3, 64, kernel_size=3, padding=1, bias=False),
                nn.BatchNorm2d(64), nn.ReLU(inplace=True))

            self.stage2_mixed = self._make_layer(block, 64, layers[0], 1, 'mixed')
            self.stage3_mixed = self._make_layer(block, 128, layers[1], 2, 'mixed')
            self.stage4_mixed = self._make_layer(block, 256, layers[2], 2, 'mixed')
            self.stage5_mixed = self._make_layer(block, 512, layers[3], 2, 'mixed')


        if self.arch=='LLC_resnet18':
            if graft == 2:
                    self.second_block_feat = nn.Sequential(
                    nn.Conv2d(64, 128, kernel_size=3, padding=1, bias=False),
                    nn.BatchNorm2d(128), nn.ReLU(inplace=True),
                    nn.AdaptiveMaxPool2d((1, 1)),
                    nn.Flatten())
                    self.second_block_last = nn.Sequential(
                    nn.Linear(128, 20))
                    self.last_layer_mixed_branch = nn.Linear(512 * block.expansion + 128, num_classes)
                    self.final_layer = nn.Linear(512 * 3 * block.expansion + 128, num_classes)


            elif graft == 3:
                    self.third_block_feat = nn.Sequential(
                    nn.Conv2d(128, 256, kernel_size=3, padding=1, bias=False),
                    nn.BatchNorm2d(256), nn.ReLU(inplace=True),
                    nn.AdaptiveMaxPool2d((1, 1)),
                    nn.Flatten())
                    self.third_block_last = nn.Sequential(
                    nn.Linear(256, 20))
                    self.last_layer_mixed_branch = nn.Linear(512 * block.expansion + 256, num_classes)
                    self.final_layer = nn.Linear(512 * 3 * block.expansion + 256, num_classes)


            elif graft == 4:
                    self.fourth_block_feat = nn.Sequential(
                    nn.Conv2d(256, 512, kernel_size=3, padding=1, bias=False),
                    nn.BatchNorm2d(512), nn.ReLU(inplace=True),
                    nn.AdaptiveMaxPool2d((1, 1)),
                    nn.Flatten())
                    self.fourth_block_last = nn.Sequential(
                    nn.Linear(512, 20))
                    self.last_layer_mixed_branch = nn.Linear(512 * block.expansion + 512, num_classes)
                    self.final_layer = nn.Linear(512 * 3 * block.expansion + 512, num_classes)


            elif graft == 5:
                    self.fifth_block_feat = nn.Sequential(
                    nn.Conv2d(512, 512, kernel_size=3, padding=1, bias=False),
                    nn.BatchNorm2d(512), nn.ReLU(inplace=True),
                    nn.AdaptiveMaxPool2d((1, 1)),
                    nn.Flatten())
                    self.fifth_block_last = nn.Sequential(
                    nn.Linear(512, 20))
                    self.last_layer_mixed_branch = nn.Linear(512 * block.expansion + 512, num_classes)
                    self.final_layer = nn.Linear(512 * 3 * block.expansion + 512, num_classes)

            self.last_layer_coarse_branch = nn.Linear(512 * block.expansion, 20)
            self.last_layer_fine_branch = nn.Linear(512 * block.expansion, num_classes)

        if self.arch=='LLC_resnet50':
            if graft == 2:
                    self.second_block_feat = nn.Sequential(
                    nn.Conv2d(64 * block.expansion, 64 * block.expansion, kernel_size=3, padding=1, bias=False),
                    nn.BatchNorm2d(64 * block.expansion), nn.ReLU(inplace=True),
                    nn.AdaptiveMaxPool2d((1, 1)),
                    nn.Flatten())
                    self.second_block_last = nn.Sequential(
                    nn.Linear(64 * block.expansion, 20))
                    self.last_layer_mixed_branch = nn.Linear(512 * block.expansion + 64 * block.expansion, num_classes)
                    self.final_layer = nn.Linear(512 * 3 * block.expansion + 64 * block.expansion, num_classes)


            elif graft == 3:
                    self.third_block_feat = nn.Sequential(
                    nn.Conv2d(128 * block.expansion, 128 * block.expansion, kernel_size=3, padding=1, bias=False),
                    nn.BatchNorm2d(128 * block.expansion), nn.ReLU(inplace=True),
                    nn.AdaptiveMaxPool2d((1, 1)),
                    nn.Flatten())
                    self.third_block_last = nn.Sequential(
                    nn.Linear(128 * block.expansion, 20))
                    self.last_layer_mixed_branch = nn.Linear(512 * block.expansion + 128 * block.expansion, num_classes)
                    self.final_layer = nn.Linear(512 * 3 * block.expansion + 128 * block.expansion, num_classes)


            elif graft == 4:
                    self.fourth_block_feat = nn.Sequential(
                    nn.Conv2d(256 * block.expansion, 256 * block.expansion, kernel_size=3, padding=1, bias=False),
                    nn.BatchNorm2d(256 * block.expansion), nn.ReLU(inplace=True),
                    nn.AdaptiveMaxPool2d((1, 1)),
                    nn.Flatten())
                    self.fourth_block_last = nn.Sequential(
                    nn.Linear(256 * block.expansion, 20))
                    self.last_layer_mixed_branch = nn.Linear(512 * block.expansion + 256 * block.expansion, num_classes)
                    self.final_layer = nn.Linear(512 * 3 * block.expansion + 256 * block.expansion, num_classes)


            elif graft == 5:
                    self.fifth_block_feat = nn.Sequential(
                    nn.Conv2d(512 * block.expansion, 512 * block.expansion, kernel_size=3, padding=1, bias=False),
                    nn.BatchNorm2d(512 * block.expansion), nn.ReLU(inplace=True),
                    nn.AdaptiveMaxPool2d((1, 1)),
                    nn.Flatten())
                    self.fifth_block_last = nn.Sequential(
                    nn.Linear(512 * block.expansion, 20))
                    self.last_layer_mixed_branch = nn.Linear(512 * block.expansion + 512 * block.expansion, num_classes)
                    self.final_layer = nn.Linear(512 * 3 * block.expansion + 512 * block.expansion, num_classes)

            self.last_layer_coarse_branch = nn.Linear(512 * block.expansion, 20)
            self.last_layer_fine_branch = nn.Linear(512 * block.expansion, num_classes)

        if self.arch=='LLC_resnet101':
            if graft == 2:
                    self.second_block_feat = nn.Sequential(
                    nn.Conv2d(64 * block.expansion, 64 * block.expansion, kernel_size=3, padding=1, bias=False),
                    nn.BatchNorm2d(64 * block.expansion), nn.ReLU(inplace=True),
                    nn.AdaptiveMaxPool2d((1, 1)),
                    nn.Flatten())
                    self.second_block_last = nn.Sequential(
                    nn.Linear(64 * block.expansion, 20))
                    self.last_layer_mixed_branch = nn.Linear(512 * block.expansion + 64 * block.expansion, num_classes)
                    self.final_layer = nn.Linear(512 * 3 * block.expansion + 64 * block.expansion, num_classes)


            elif graft == 3:
                    self.third_block_feat = nn.Sequential(
                    nn.Conv2d(128 * block.expansion, 128 * block.expansion, kernel_size=3, padding=1, bias=False),
                    nn.BatchNorm2d(128 * block.expansion), nn.ReLU(inplace=True),
                    nn.AdaptiveMaxPool2d((1, 1)),
                    nn.Flatten())
                    self.third_block_last = nn.Sequential(
                    nn.Linear(128 * block.expansion, 20))
                    self.last_layer_mixed_branch = nn.Linear(512 * block.expansion + 128 * block.expansion, num_classes)
                    self.final_layer = nn.Linear(512 * 3 * block.expansion + 128 * block.expansion, num_classes)


            elif graft == 4:
                    self.fourth_block_feat = nn.Sequential(
                    nn.Conv2d(256 * block.expansion, 256 * block.expansion, kernel_size=3, padding=1, bias=False),
                    nn.BatchNorm2d(256 * block.expansion), nn.ReLU(inplace=True),
                    nn.AdaptiveMaxPool2d((1, 1)),
                    nn.Flatten())
                    self.fourth_block_last = nn.Sequential(
                    nn.Linear(256 * block.expansion, 20))
                    self.last_layer_mixed_branch = nn.Linear(512 * block.expansion + 256 * block.expansion, num_classes)
                    self.final_layer = nn.Linear(512 * 3 * block.expansion + 256 * block.expansion, num_classes)



            elif graft == 5:
                    self.fifth_block_feat = nn.Sequential(
                    nn.Conv2d(512 * block.expansion, 512 * block.expansion, kernel_size=3, padding=1, bias=False),
                    nn.BatchNorm2d(512 * block.expansion), nn.ReLU(inplace=True),
                    nn.AdaptiveMaxPool2d((1, 1)),
                    nn.Flatten())
                    self.fifth_block_last = nn.Sequential(
                    nn.Linear(512 * block.expansion, 20))
                    self.last_layer_mixed_branch = nn.Linear(512 * block.expansion + 512 * block.expansion, num_classes)
                    self.final_layer = nn.Linear(512 * 3 * block.expansion + 512 * block.expansion, num_classes)

            self.last_layer_coarse_branch = nn.Linear(512 * block.expansion, 20)
            self.last_layer_fine_branch = nn.Linear(512 * block.expansion, num_classes)

        if self.arch=='LLC_resnet152':
            if graft == 2:
                self.second_block_feat = nn.Sequential(
                    nn.Conv2d(64 * block.expansion, 64 * block.expansion, kernel_size=3, padding=1, bias=False),
                    nn.BatchNorm2d(64 * block.expansion), nn.ReLU(inplace=True),
                    nn.AdaptiveMaxPool2d((1, 1)),
                    nn.Flatten())
                self.second_block_last = nn.Sequential(
                    nn.Linear(64 * block.expansion, 20))
                self.last_layer_mixed_branch = nn.Linear(512 * block.expansion + 64 * block.expansion, num_classes)
                self.final_layer = nn.Linear(512 * 3 * block.expansion + 64 * block.expansion, num_classes)


            elif graft == 3:
                self.third_block_feat = nn.Sequential(
                    nn.Conv2d(128 * block.expansion, 128 * block.expansion, kernel_size=3, padding=1, bias=False),
                    nn.BatchNorm2d(128 * block.expansion), nn.ReLU(inplace=True),
                    nn.AdaptiveMaxPool2d((1, 1)),
                    nn.Flatten())
                self.third_block_last = nn.Sequential(
                    nn.Linear(128 * block.expansion, 20))
                self.last_layer_mixed_branch = nn.Linear(512 * block.expansion + 128 * block.expansion, num_classes)
                self.final_layer = nn.Linear(512 * 3 * block.expansion + 128 * block.expansion, num_classes)


            elif graft == 4:
                self.fourth_block_feat = nn.Sequential(
                    nn.Conv2d(256 * block.expansion, 256 * block.expansion, kernel_size=3, padding=1, bias=False),
                    nn.BatchNorm2d(256 * block.expansion), nn.ReLU(inplace=True),
                    nn.AdaptiveMaxPool2d((1, 1)),
                    nn.Flatten())
                self.fourth_block_last = nn.Sequential(
                    nn.Linear(256 * block.expansion, 20))
                self.last_layer_mixed_branch = nn.Linear(512 * block.expansion + 256 * block.expansion, num_classes)
                self.final_layer = nn.Linear(512 * 3 * block.expansion + 256 * block.expansion, num_classes)


            elif graft == 5:
                self.fifth_block_feat = nn.Sequential(
                    nn.Conv2d(512 * block.expansion, 512 * block.expansion, kernel_size=3, padding=1, bias=False),
                    nn.BatchNorm2d(512 * block.expansion), nn.ReLU(inplace=True),
                    nn.AdaptiveMaxPool2d((1, 1)),
                    nn.Flatten())
                self.fifth_block_last = nn.Sequential(
                    nn.Linear(512 * block.expansion, 20))
                self.last_layer_mixed_branch = nn.Linear(512 * block.expansion + 512 * block.expansion, num_classes)
                self.final_layer = nn.Linear(512 * 3 * block.expansion + 512 * block.expansion, num_classes)

            self.last_layer_coarse_branch = nn.Linear(512 * block.expansion, 20)
            self.last_layer_fine_branch = nn.Linear(512 * block.expansion, num_classes)


    def _make_layer(self, block, out_channels, num_blocks, stride, which_branch):
        """make resnet layers(by layer i didnt mean this 'layer' was the 
        same as a neuron netowork layer, ex. conv layer), one layer may 
        contain more than one residual block 
        Args:
            block: block type, basic block or bottle neck block
            out_channels: output depth channel number of this layer
            num_blocks: how many blocks per layer
            stride: the stride of the first block of this layer
        
        Return:
            return a resnet layer
        """

        strides = [stride] + [1] * (num_blocks - 1)
        layers = []
        if which_branch =='branch_1':
            for stride in strides:
                layers.append(block(self.in_channels_1, out_channels, stride))
                self.in_channels_1 = out_channels * block.expansion
        elif which_branch =='branch_2':
            for stride in strides:
                layers.append(block(self.in_channels_2, out_channels, stride))
                self.in_channels_2 = out_channels * block.expansion
        elif which_branch =='mixed':
            for stride in strides:
                layers.append(block(self.in_channels_mixed, out_channels, stride))
                self.in_channels_mixed = out_channels * block.expansion

        return nn.Sequential(*layers)

    def forward(self, data):
        if self.arch == 'resnet18' or self.arch == 'resnet50' or self.arch == 'resnet101' or  self.arch =='resnet152':
            x = self.conv1_1branch(data)
            x = self.stage2_1branch(x)
            x = self.stage3_1branch(x)
            x = self.stage4_1branch(x)
            x = self.stage5_1branch(x)
            x = self.avg_pool(x)
            x = x.view(x.size(0), -1)
            output = self.last_layer(x)
            return output

        elif self.arch =='LLC_resnet18' or self.arch =='LLC_resnet50' or self.arch =='LLC_resnet101' or self.arch =='LLC_resnet152':
            x = self.conv1_1branch(data)
            x_2 = self.conv1_2branch(data)
            x_3 = self.conv1_branch_mixed(data)

            x = self.stage2_1branch(x)
            x_2 = self.stage2_2branch(x_2)
            x_3= self.stage2_mixed(x_3)

            if self.graft == 2:
                mixed_block_feat = self.second_block_feat(x_3)
                mixed_coarse_layer = self.second_block_last(mixed_block_feat)


            x = self.stage3_1branch(x)
            x_2 = self.stage3_2branch(x_2)
            x_3= self.stage3_mixed(x_3)


            if self.graft == 3:
                mixed_block_feat = self.third_block_feat(x_3)
                mixed_coarse_layer = self.third_block_last(mixed_block_feat)

            x = self.stage4_1branch(x)
            x_2 = self.stage4_2branch(x_2)
            x_3= self.stage4_mixed(x_3)

            if self.graft == 4:
                mixed_block_feat = self.fourth_block_feat(x_3)
                mixed_coarse_layer = self.fourth_block_last(mixed_block_feat)

            x = self.stage5_1branch(x)
            x_2 = self.stage5_2branch(x_2)
            x_3= self.stage5_mixed(x_3)

            if self.graft == 5:
                mixed_block_feat = self.fifth_block_feat(x_3)
                mixed_coarse_layer = self.fifth_block_last(mixed_block_feat)

            x = self.avg_pool(x)
            x_2 = self.avg_pool(x_2)
            x_3 = self.avg_pool(x_3)

            x = x.view(x.size(0), -1)
            x_2 = x_2.view(x_2.size(0), -1)
            x_3 = x_3.view(x_3.size(0), -1)

            mixed_concat=torch.cat((x_3, mixed_block_feat), dim=1)
            final_concat=torch.cat((x, x_2, mixed_concat), dim=1)

            out_fine_branch = self.last_layer_fine_branch(x)
            out_coarse_branch=self.last_layer_coarse_branch(x_2)
            out_mixed_branch = self.last_layer_mixed_branch(mixed_concat)
            final_output = self.final_layer(final_concat)
            return final_output, out_coarse_branch, out_mixed_branch, mixed_coarse_layer, out_fine_branch

def _resnet(arch, block, layers, pretrained, progress, graft, **kwargs):
    model = ResNet(block, layers, arch, graft, **kwargs)
    # only load state_dict()
    if pretrained:
        model.load_state_dict(
            torch.load(model_urls[arch], map_location=torch.device('cpu')))
    return model


def resnet18(arch, graft, pretrained=False, progress=True, **kwargs):
    return _resnet(arch, BasicBlock, [2, 2, 2, 2], pretrained, progress, graft,
                   **kwargs)


def resnet34(arch,graft,pretrained=False, progress=True, **kwargs):
    return _resnet(arch, BasicBlock, [3, 4, 6, 3], pretrained, progress, graft,
                   **kwargs)


def resnet50(arch, graft,pretrained=False, progress=True, **kwargs):
    return _resnet(arch, BottleNeck, [3, 4, 6, 3], pretrained, progress, graft,
                   **kwargs)


def resnet101(arch, graft, pretrained=False, progress=True, **kwargs):
    return _resnet(arch, BottleNeck, [3, 4, 23, 3], pretrained,
                   progress, graft, **kwargs)


def resnet152(arch, graft, pretrained=False, progress=True, **kwargs):
    return _resnet(arch, BottleNeck, [3, 8, 36, 3], pretrained,
                   progress, graft, **kwargs)