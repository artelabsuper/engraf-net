import os
import re
import numpy as np
from matplotlib import pyplot as plt
from matplotlib.pyplot import subplots, close
import utils


def plot_bar(data, title):
    fig, ax = subplots()
    domain=data.shape[0]
    for idx, i in enumerate(data.T):
        ax.plot(i , ls='-', label='Intra '+str(idx),fillstyle='none')

    ax.set(xlabel='Domain', ylabel='Range',
           title=title)
    #xticks(domain)
    ax.grid()
    #legend(loc='upper right')
    fig.savefig(path_bar+title+'.png', dpi=200)
    plt.close(fig)
    #show(bbox_inches='tight', dpi=400)



def plot_loss_epochs(title, ylabel):
    fig, ax = plt.subplots(1,1)
    for index, i in enumerate(acc_list):
        if i[1]=='xent':
            c='orange'
            ax.plot(domain, i[2][start_domain:end_domain:step], label='xent', linewidth=3, c=c)
        elif i[1]=='center':
            c='blue'
            ax.plot(domain, i[2][start_domain:end_domain:step], label='Cent loss', linewidth=3, c=c)
            ax.plot(domain, i[3][start_domain:end_domain:step], '--', label='Xent', linewidth=3, c=c)
            ax.plot(domain, [x + y for x, y in zip(i[2][start_domain:end_domain:step], i[3][start_domain:end_domain:step])], '+', label='Xent+Center', linewidth=1, c=c)
        elif i[1] == 'GMC':
            c='red'
            ax.plot(domain, i[2][start_domain:end_domain:step], label='GMC loss', linewidth=3, c=c, zorder=1)
            ax.plot(domain, i[3][start_domain:end_domain:step], '--', label='Xent', linewidth=3, c=c, zorder=1)
            ax.plot(domain, [x + y for x, y in zip(i[2][start_domain:end_domain:step], i[3][start_domain:end_domain:step])], '+', label='Xent+GMC', linewidth=1, c=c)


    plt.tick_params(labelsize=20)
    plt.xlabel('Epochs', fontsize=18)
    plt.ylabel(ylabel, fontsize=18)
    ax.grid()
    #plt.suptitle(title, fontsize=20)
    plt.legend(loc='upper right',prop={'size': 20})
    #show(bbox_inches='tight', dpi=400)    #plt.show(dpi=400)
    #dirname = osp.join(args.save_dir)
    plt.tight_layout()
    fig.savefig('/home/nataraja/Desktop/'+title+'.png', dpi=500)
    close()


def plot_accuracy_epochs(title, ylabel):
    fig, ax = plt.subplots(1,1)
    for index, i in enumerate(acc_list):
        if i[1]=='xent':
            c='green'
            ax.plot(domain, i[2][start_domain:end_domain:step],'--', marker='^', label='xent', linewidth=2, c=c)
        elif i[1] == 'two_branch_exp':
            c='red'
            ax.plot(domain, i[2][start_domain:end_domain:step], marker='s', label='two_b', linewidth=2, c=c, zorder=1)
        elif i[1] == '1 branch graft4':
            c='blue'
            ax.plot(domain, i[2][start_domain:end_domain:step], marker='s', label='1b_graft4', linewidth=2, c=c, zorder=1)
        elif i[1] == 'proposal_g=2':
            c='black'
            ax.plot(domain, i[2][start_domain:end_domain:step], marker='s', label='graft2', linewidth=2, c=c, zorder=1)
        elif i[1] == 'proposal_g=3':
            c='orange'
            ax.plot(domain, i[2][start_domain:end_domain:step], marker='s', label='graft3', linewidth=2, c=c, zorder=1)
        elif i[1] == 'proposal_g=4':
            c='red'
            ax.plot(domain, i[2][start_domain:end_domain:step], marker='s', label='EnGraf-Net152', linewidth=2, c=c, zorder=1)
        elif i[1] == 'proposal_g=5':
            c='brown'
            ax.plot(domain, i[2][start_domain:end_domain:step], marker='s', label='graft5', linewidth=2, c=c, zorder=1)

    plt.tick_params(labelsize=20)
    plt.xlabel('Epochs', fontsize=18)
    plt.ylabel(ylabel+'(%)', fontsize=18)
    ax.grid()
    plt.rc('font', family='serif')
    #plt.title(title)
    plt.legend(loc='lower right', prop={'size': 14})
    plt.tight_layout()
    # show(bbox_inches='tight', dpi=400)    #plt.show(dpi=400)
    # dirname = osp.join(args.save_dir)
    fig.savefig('/home/nataraja/Desktop/' + title + '.png', dpi=500)
    close()

# comparison_files=['Exp:1_log_cifar10_gpu= _loss=center_loss', 'Exp:1_log_cifar10_gpu= 0_k_nor=4_knn=4_range_f=4','Exp:1_log_cifar10_gpu= 0_k_nor=12_knn=4_range_f=4','Exp:1_log_cifar10_gpu= 0_k_nor=12_knn=4_range_f=2']

marker = ['+', '.', '1','2']
lines = ["-","--","-.",":"]
#path="/home/nataraja/Desktop/Multiple Anchors images/cifar10_new_exp/"
path='/home/nataraja/Desktop/HDL_v4Exp/CUB/Resnet-152/' #FGVC
path_bar="/home/nataraja/Pictures/potential_loss/intravariance/101/"
utils.mkdir_if_missing(path_bar)
list_dir=os.listdir(path)
regex = ('Accuracy ') + '\(%\): \d+.\d+'
regex_intra = ('Class  ')+('\d+ :  \d+ Std:  \d+.\d+')
regex_cent = ('CrossEntropy ') + '\d+.\d+ \(\d+.\d+\)'+(' CenterLoss ') + '\d+.\d+ \(\d+.\d+\)'
regex_GMC = ('CrossEntropy ') + '\d+.\d+ \(\d+.\d+\)' + (' CGM_loss ') + '\d+.\d+ \(\d+.\d+\)'
#regex_sigmoid_parameters = re.compile('K_normalizer:  \[\[+( ? ? ? ? ?-?\d+.\d+(?:[e]\ *[-|+]?\ *\d+)? ? ? ? ?\n?\]?\]?)+')
acc_list = []

start_domain=10
step=15
end_domain=150
domain=[i for i in range(start_domain,end_domain,step)]
#
for i in sorted(list_dir):
    file_list = os.listdir(path + '/' + i + '/')
    for idx, j in enumerate(sorted(file_list)):
        filename = j.split('_')
        F=(open(path+i+'/'+j,'r'))
        #if i=='GMC' and j=='Exp:7_log_cifar100_gpu= 1_loss=CGM_loss_lambda=0.01': #best file with max accuracy achieved
        if i=='DGMC':# and j=='Exp:1_log_datasets-nas_gpu= 1_loss=CGM_loss_lambda=0.01': #best file with max accuracy achieved
            match = re.findall(regex, F.read())
            tmp=[float((j.split()[2])) for j in match]
            acc_list.append([filename[4]+filename[5],i, tmp])
            F.close()
        #elif i=='center' and j=='Exp:5_log_cifar100_gpu= 0_loss=center_loss_lambda=0.01_lr_cent=0.001':
        elif i == 'center' and j == 'Exp:2_log_datasets_gpu= 1_loss=center_loss_lambda=0.01_lr_cent=0.001':
            match = re.findall(regex, F.read())
            tmp=[float((j.split()[2])) for j in match]
            acc_list.append([filename[4]+filename[5],i, tmp])
            F.close()

        #elif i=='xent' and j=='Exp:5_log_cifar100_gpu= 1_knn=_loss=xent_loss':
        elif i == 'xent' :#and j == 'Exp:8_log_datasets_gpu= 0_knn=_loss=xent_loss':
            match = re.findall(regex, F.read())
            tmp=[float((j.split()[2])) for j in match]
            acc_list.append([filename[4]+filename[5],i, tmp])
            F.close()

        elif i == 'two_branch_exp' :#and j == 'Exp:8_log_datasets_gpu= 0_knn=_loss=xent_loss':
            match = re.findall(regex, F.read())
            tmp=[float((j.split()[2])) for j in match]
            acc_list.append([filename[4]+filename[5],i, tmp])
            F.close()
        elif i == '1 branch graft4' :#and j == 'Exp:8_log_datasets_gpu= 0_knn=_loss=xent_loss':
            match = re.findall(regex, F.read())
            tmp=[float((j.split()[2])) for j in match]
            acc_list.append([filename[4]+filename[5],i, tmp])
            F.close()
        elif i == 'proposal_g=2' :#and j == 'Exp:8_log_datasets_gpu= 0_knn=_loss=xent_loss':
            match = re.findall(regex, F.read())
            tmp=[float((j.split()[2])) for j in match]
            acc_list.append([filename[4]+filename[5],i, tmp])
            F.close()
        elif i == 'proposal_g=3' :#and j == 'Exp:8_log_datasets_gpu= 0_knn=_loss=xent_loss':
            match = re.findall(regex, F.read())
            tmp=[float((j.split()[2])) for j in match]
            acc_list.append([filename[4]+filename[5],i, tmp])
            F.close()
        elif i == 'proposal_g=4' :#and j == 'Exp:8_log_datasets_gpu= 0_knn=_loss=xent_loss':
            match = re.findall(regex, F.read())
            tmp=[float((j.split()[2])) for j in match]
            acc_list.append([filename[4]+filename[5],i, tmp])
            F.close()
        elif i == 'proposal_g=5' :#and j == 'Exp:8_log_datasets_gpu= 0_knn=_loss=xent_loss':
            match = re.findall(regex, F.read())
            tmp=[float((j.split()[2])) for j in match]
            acc_list.append([filename[4]+filename[5],i, tmp])
            F.close()




plot_accuracy_epochs('Accuracies', 'Accuracy')

# for i in list_dir:
#     k_list=[]
#     K_normalizer_cosine, range_f_cosine= [],[]
#     filename = i.split('_')
#     for ix,match in enumerate(re.finditer(regex_sigmoid_parameters, open(path+i,'r').read())):
#         tmp=match.group().split()
#         if '[[' in tmp:
#             tmp.remove('[[')
#         if 'K_normalizer:' in tmp:
#             tmp.remove('K_normalizer:')
#         tmp[-1]=tmp[-1].replace(']]','')
#         tmp[0] = tmp[0].replace('[[', '')
#         tmp[-1] = tmp[0].replace('\'\'', '')
#         k_list.append([float(j) for j in tmp])
#
#     k_transpose=list((map(list, zip(*k_list))))
#     plot_accuracy_epochs(filename[0]+filename[3]+filename[4]+' '+filename[7])
#
#
#
#     #for i in match:
#         #K_normalizer_cosine.append(40 * (1 / (1 + np.math.exp(-K_normalizer))) + 5)
#         #acc_list.append([filename[0]+'_0', K_normalizer_cosine[10:]])
#     # k_list_np=np.array(k_list)
#     # exit()
#     # plot_accuracy_epochs()
#     # exit()

#

#Print accuracy
dict={'cifar10':10, 'cifar100':100, 'fashion':10, '101':102, 'dataset':3} #TODO change this for PINS

for i in (sorted(list_dir)):
    file_list = os.listdir(path+'/'+i+'/')
    for idx,j in enumerate(sorted(file_list)):
        match = re.findall(regex, open(path+i+'/'+j,'r').read())
        if match:
            filename=j.split('_',3)[2]
            acc_list=[]
            #tmp_intra=[float((i.split(': ')[2])) for i in match]
            #acc_list.append(tmp_intra)
            #np.set_printoptions(threshold=sys.maxsize)
            #match_acc = re.findall(regex, open(path+folder+'/'+i,'r').read())
            tmp_acc=[float((j.split(': ')[1])) for j in match]
            #print(i, (np.mean(tmp_intra[len(tmp_intra):len(tmp_intra) - dict.get(filename)-1:-1])), tmp_acc[-1],"Max: ",np.max(tmp_acc))
            print(j,i, "Last: ",tmp_acc[-1], "--> Max: ",np.max(tmp_acc), "Avg last 5: ",np.average(tmp_acc[-1:len(tmp_acc)-5:-1]))
#
#             #plot_bar(np.asarray(tmp_intra).reshape(-1, dict.get(filename)),i)


#########################
# Error Plot for Cifar-100

# acc_list = []
# for i in sorted(list_dir):
#     file_list = os.listdir(path + '/' + i + '/')
#     for idx, j in enumerate(sorted(file_list)):
#         filename = j.split('_')
#         F=(open(path+i+'/'+j,'r'))
#         if i=='GMC' and j=='Exp:1_log_datasets-nas_gpu= 1_loss=CGM_loss_lambda=0.01': #best file with max accuracy achieved
#         #if i == 'GMC' and j == 'Exp:7_log_cifar100_gpu= 1_loss=CGM_loss_lambda=0.01':
#             match = re.findall(regex_GMC, F.read())
#             tmp_GMC=[float((j.split()[5]).replace(('('),'').replace((')'),'')) for j in match]
#             tmp_xent=[float((j.split()[2]).replace(('('),'').replace((')'),'')) for j in match]
#             acc_list.append([filename[4]+filename[5],i, tmp_GMC, tmp_xent])
#             F.close()
#         elif i=='center' and j=='Exp:2_log_datasets_gpu= 1_loss=center_loss_lambda=0.01_lr_cent=0.001':
#         #elif i == 'center' and j == 'Exp:5_log_cifar100_gpu= 0_loss=center_loss_lambda=0.01_lr_cent=0.001':
#             match = re.findall(regex_cent, F.read())
#             tmp_center=[float((j.split()[5]).replace(('('),'').replace((')'),'')) for j in match]
#             tmp_xent=[float((j.split()[2]).replace(('('),'').replace((')'),'')) for j in match]
#             acc_list.append([filename[4] + filename[5], i, tmp_center, tmp_xent])
#             F.close()
#
#
# plot_loss_epochs('Loss comparison', 'Error')