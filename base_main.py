import argparse
import datetime
import os
import os.path as osp
import sys
import time
import matplotlib
import json
import torchvision.models as models
from utils import Logger, AverageMeter
from MLP import MLP
matplotlib.use('Agg')
from matplotlib import pyplot as plt
import numpy as np
import torch
import torch.backends.cudnn as cudnn
import torch.nn as nn
from torch.optim import lr_scheduler
import dataset_load
import utils



def main(args, trainloader, testloader, num_classes, file_output):
    best_acc = 0.
    str_model=args.model
    dataset_name = args.path.split('/')[-2]
    if args.loss =='LLC_loss':
        if dataset_name == 'CUB_200_2011':
            from EnGrafNet_pre import Ext_ResNet
            model = Ext_ResNet(str_model, args.graft, num_classes=num_classes)
        elif dataset_name =='cifar100':
            if str_model == 'LLC_resnet18':
                from resnet_model import resnet18
                model = resnet18(str_model,args.graft,num_classes=num_classes)
            elif str_model == 'LLC_resnet50':
                from resnet_model import resnet50
                model = resnet50(str_model,args.graft, num_classes=num_classes)
            elif str_model == 'LLC_resnet101':
                from resnet_model import resnet101
                model = resnet101(str_model,args.graft, num_classes=num_classes)
            elif str_model == 'LLC_resnet152':
                from resnet_model import resnet152
                model = resnet152(str_model,args.graft, num_classes=num_classes)
        elif dataset_name == 'fgvc_dataset':
            from EnGrafNet_pre import Ext_ResNet
            model = Ext_ResNet(str_model, args.graft, num_classes=num_classes)
        else:
            print("Error select dataset, please modify the path parameter")
            print("Dataset name extracted: ", dataset_name)
            exit()

    elif args.loss =='xent_loss':
        if dataset_name =='cifar100':
            if str_model == 'resnet18':
                from resnet_model import resnet18
                model = resnet18(str_model,None, num_classes=num_classes)
            elif str_model == 'resnet50':
                from resnet_model import resnet50
                model = resnet50(str_model, None, num_classes=num_classes)
            elif str_model == 'resnet101':
                from resnet_model import resnet101
                model = resnet101(str_model, None,num_classes=num_classes)
            elif str_model == 'resnet152':
                from resnet_model import resnet152
                model = resnet152(str_model, None, num_classes=num_classes)
        else:
            print("You have to use cifar100 with baseline or LLC")
            print("You can use LLC from pretrained model on CUB200 and not the baseline!")
            print("The baseline from scratch on CUB200 is in the file resnet_scratchCUB.py")
            print("abort")
            exit()

    model = nn.DataParallel(model.cuda())

    criterion_xent = nn.CrossEntropyLoss()
    criterion_xent_mixed = nn.CrossEntropyLoss()
    criterion_xent_coarse = nn.CrossEntropyLoss()
    criterion_xent_fine = nn.CrossEntropyLoss()
    criterion_xent_coarse_mixed = nn.CrossEntropyLoss()

    params = list(model.parameters())
    if args.optimizer =='adam':
        #print("Adam optimizer", file=file_output)
        optimizer = torch.optim.Adam(params, lr=args.lr)
    elif args.optimizer =='sgd':
        optimizer = torch.optim.SGD(params, lr=args.lr, weight_decay=1e-4, momentum=0.9)
        #print("SGD Optimizer weight_decay=1e-4, momentum=0.9", file=file_output)

    #scheduler = lr_scheduler.CosineAnnealingLR(optimizer, args.max_epoch)
    scheduler = torch.optim.lr_scheduler.StepLR(optimizer, step_size=30, gamma=0.1)
    ##MultiStepLR(raw_optimizer, milestones=[60, 100], gamma=0.1),

    start_time = time.time()
    for epoch in range(args.max_epoch):
        train(trainloader, model, criterion_xent, criterion_xent_coarse,criterion_xent_mixed,criterion_xent_coarse_mixed,
                  criterion_xent_fine,optimizer, args, file_output)

        print("==> Epoch {}/{}".format(epoch + 1, args.max_epoch), " Lr: ", utils.get_lr(optimizer), file=file_output)

        scheduler.step()
        #utils.adjust_learning_rate(optimizer, epoch, args)

        print("==> Test", file=file_output)
        if args.loss == 'LLC_loss':
            acc, err, coarse_acc, coarse_err, mixed_acc, mixed_err, \
            coarse_mixed_acc, coarse_mixed_err, fine_acc, fine_err = test(model, testloader, args)
            print("Accuracy (%): {:.4f}\t Error rate (%): {:.4f} "
                  "Accuracy LLC_loss coarse (%): {:.4f}\t Error rate (%): {:.4f} "
                  "Accuracy LLC_loss mixed (%): {:.4f}\t Error rate (%): {:.4f} "
                  "Accuracy LLC_loss coarse mixed (%): {:.4f}\t Error rate (%): {:.4f} "
                  "Accuracy LLC_loss fine (%): {:.4f}\t Error rate (%): {:.4f}".format(acc, err, coarse_acc, coarse_err, mixed_acc,
                                                                                       mixed_err, coarse_mixed_acc, coarse_mixed_err,
                                                                                       fine_acc, fine_err), file=file_output)
        elif args.loss =='xent_loss':
            acc, err = test(model, testloader, args)
            print("Accuracy (%): {:.4f}\t Error rate (%): {:.4f} ".format(acc, err),file=file_output)

        if args.save_model and acc > best_acc:
            saved_model_path = osp.join(args.save_dir,'model_w_' + args.model + 'dat_' + dataset_name + '_' + args.loss)
            torch.save(model.state_dict(), saved_model_path)
            print("Model saved")
            best_acc = acc

    elapsed = round(time.time() - start_time)
    elapsed = str(datetime.timedelta(seconds=elapsed))
    print("Finished. Total elapsed time (h:m:s): {}".format(elapsed),file=file_output)
    return True
    #plot_accuracy_epochs(acc_list)
    #save_list(acc_list)



def train(trainloader, model, criterion_xent, criterion_xent_coarse,criterion_xent_mixed,criterion_xent_coarse_mixed,
          criterion_xent_fine, optimizer,args, file_output):
    model.train()
    xent_losses = AverageMeter()
    xent_coarse = AverageMeter()
    xent_fine = AverageMeter()
    xent_mixed = AverageMeter()
    xent_coarse_mixed = AverageMeter()



    if args.loss == 'LLC_loss':
        for batch_idx, (data, labels, cl0) in enumerate(trainloader):
            data, labels, cl0 = data.cuda(), labels.cuda(), cl0.cuda()
            outputs, out_coarse_branch, out_mixed_branch, out_mixed_coarse_layer, out_fine_branch = model(data)
            loss_xent = criterion_xent(outputs, labels)
            loss_xent_fine = criterion_xent_fine(out_fine_branch, labels)
            loss_xent_coarse = criterion_xent_coarse(out_coarse_branch, cl0)
            loss_xent_mixed = criterion_xent_mixed(out_mixed_branch, labels)
            loss_xent_coarse_mixed = criterion_xent_coarse_mixed(out_mixed_coarse_layer, cl0)
            loss = loss_xent + loss_xent_coarse + loss_xent_mixed + loss_xent_coarse_mixed + loss_xent_fine
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            xent_losses.update(loss_xent.item(), labels.size(0))
            xent_coarse.update(loss_xent_coarse.item(), cl0.size(0))
            xent_fine.update(loss_xent_fine.item(), labels.size(0))
            xent_mixed.update(loss_xent_mixed.item(), labels.size(0))
            xent_coarse_mixed.update(loss_xent_coarse_mixed.item(), cl0.size(0))

            if (batch_idx + 1) % 50 == 0:
                    print("Batch {}\t Last Xent Loss {:.6f} ({:.6f}) "
                          "LLC coarse Loss {:.6f} ({:.6f}) "
                          "LLC mixed Loss {:.6f} ({:.6f}) "
                          "LLC coarse mixed Loss {:.6f} ({:.6f}) "
                          "LLC fine Loss {:.6f} ({:.6f})".format(
                            batch_idx + 1,
                            xent_losses.val,xent_losses.avg,
                            xent_coarse.val,xent_coarse.avg,
                            xent_mixed.val,xent_mixed.avg,
                            xent_coarse_mixed.val, xent_coarse_mixed.avg,
                            xent_fine.val, xent_fine.avg),file=file_output)

    elif args.loss=='xent_loss':
        for batch_idx,(data, labels) in enumerate(trainloader):
            data, labels = data.cuda(), labels.cuda()
            outputs = model(data)
            loss_xent = criterion_xent(outputs, labels)
            loss = loss_xent
            optimizer.zero_grad()
            loss.backward()
            optimizer.step()
            xent_losses.update(loss_xent.item(), labels.size(0))

            if (batch_idx + 1) % 50 == 0:
                print("Batch {}\t CrossEntropy {:.6f} ({:.6f})".format(batch_idx + 1, xent_losses.val,xent_losses.avg),file=file_output)


def test(model, testloader, args):
    model.eval()
    right = 0
    wrong = 0
    nothing = 0
    #confusion_matrix = np.zeros((107,107), dtype=int)
    correct, total, coarse_correct, mixed_correct, coarse_mixed_correct, fine_correct, \
    total_coarse, total_mixed, total_coarse_mixed, total_fine = 0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    with torch.no_grad():
        if args.loss == 'LLC_loss':
            for data, labels, cl0 in testloader:
                data, labels, cl0 = data.cuda(), labels.cuda(), cl0.cuda()
                outputs, out_coarse_branch, out_mixed_branch, out_mixed_coarse_layer, out_fine_branch = model(data)

                _, predicted = torch.max(outputs.data, 1)
                total += labels.size(0)
                bool_predicted = (predicted == labels)
                correct += bool_predicted.sum().float().item()

                _, predicted_coarse = torch.max(out_coarse_branch.data, 1)
                _, predicted_fine = torch.max(out_fine_branch.data, 1)
                _, predicted_mixed = torch.max(out_mixed_branch.data, 1)
                _, predicted_coarse_mixed = torch.max(out_mixed_coarse_layer.data, 1)

                total_coarse += cl0.size(0)
                total_mixed += labels.size(0)
                total_fine += labels.size(0)
                total_coarse_mixed+=cl0.size(0)

                bool_predicted_coarse = (predicted_coarse == cl0)
                bool_predicted_fine = (predicted_fine == labels)
                bool_predicted_mixed = (predicted_mixed == labels)
                bool_predicted_coarse_mixed = (predicted_coarse_mixed == cl0)

                coarse_correct += bool_predicted_coarse.sum().float().item()
                fine_correct += bool_predicted_fine.sum().float().item()
                mixed_correct += bool_predicted_mixed.sum().float().item()
                coarse_mixed_correct += bool_predicted_coarse_mixed.sum().float().item()

        elif args.loss == 'xent_loss':
            for data, labels in testloader:
                data, labels = data.cuda(), labels.cuda()
                outputs = model(data)
                _, predicted = torch.max(outputs.data, 1)
                total += labels.size(0)
                bool_predicted = (predicted == labels)
                correct += bool_predicted.sum().float().item()

    #         for i, j in zip(bool_predicted, bool_predicted1):
    #             i,j=i.item(), j.item()
    #             if i == False and j == True:
    #                 right += 1
    #             elif i == True and j == False:
    #                 wrong += 1
    #             else:
    #                 nothing += 1
    #
    #
    #         for t, p in zip(predicted_clusters0, s_labels):
    #             try:
    #                 confusion_matrix[int(t), int(p)] += 1
    #             except Exception:
    #                 print('Error',t,p)
    #                 exit()
    #
    # for z in range(confusion_matrix.shape[0]):
    #     print("Precision class: ",z, " ->",confusion_matrix[z][z]/confusion_matrix[z].sum())
    #
    # print("Report: ", right, wrong, nothing)

    if args.loss == 'xent_loss':
        acc = correct * 100. / total
        err = 100. - acc
        return acc, err
    elif args.loss == 'LLC_loss':
        acc = correct * 100. / total
        err = 100. - acc
        coarse_acc = coarse_correct * 100. / total_coarse
        coarse_err = 100. - coarse_acc
        fine_acc = fine_correct * 100. / total_fine
        fine_err = 100. - fine_acc
        mixed_acc = mixed_correct * 100. / total_mixed
        mixed_err = 100. - mixed_acc
        coarse_mixed_acc = coarse_mixed_correct * 100. / total_coarse_mixed
        coarse_mixed_err = 100. - coarse_mixed_acc
        return acc, err, coarse_acc, coarse_err, mixed_acc, mixed_err, coarse_mixed_acc, coarse_mixed_err, fine_acc, fine_err


if __name__ == '__main__':
    main()
