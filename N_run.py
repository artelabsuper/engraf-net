import json
import os
import os.path as osp
import torch
from torch.backends import cudnn
import argparse

import base_main
import dataset_load
from utils import Logger


def args_parser():
    parser = argparse.ArgumentParser()
    parser.add_argument('-d', '--dataset', type=str, default='MyDataset')
    parser.add_argument('-j', '--workers', default=4, type=int)
    parser.add_argument('--batch-size', type=int, default=128)
    parser.add_argument('--lr', type=float, default=0.001, help="learning rate for model")
    parser.add_argument('--max-epoch', type=int, default=500)
    parser.add_argument('--model', type=str, default='resnet18')
    parser.add_argument('--eval-freq', type=int, default=1)
    parser.add_argument('--print-freq', type=int, default=50)
    parser.add_argument('--gpu', type=str, default='0')
    parser.add_argument('--seed', type=int, default=1)
    parser.add_argument('--save-dir', type=str, default='log')
    parser.add_argument('--path', type=str, default='dataset')
    parser.add_argument('--save_model', type=str, default='False', help="save model")
    parser.add_argument('--plot', action='store_true', help="whether to plot features for every epoch")
    parser.add_argument('--loss', type=str, default='xent_loss')
    parser.add_argument('--graft', type=int, default=0)
    parser.add_argument('--optimizer', type=str, default='adam')
    parser.add_argument('--path_annotation', type=str, default='None')
    parser.add_argument('--pretrained', type=str, default='False', help="load pretrained model")
    parser.add_argument('--balanced_classes', type=int, default=3)
    return parser.parse_args()

args=args_parser()
os.environ['CUDA_VISIBLE_DEVICES'] = args.gpu
use_gpu = torch.cuda.is_available()
if use_gpu:
    #print("Pretrained model: {:}".format(args.pretrained))
    cudnn.benchmark = True
    cudnn.enabled = True
    torch.cuda.manual_seed_all(args.seed)
else:
    print("Currently using CPU")
    exit()

print("Let's use", torch.cuda.device_count(), "GPUs!")
# str_dataset=args.path.split('/')[-1]
str_dataset = args.path.split('/')[-2]

if args.dataset=='eggsmentations' or args.dataset=='MyDataset':
    kf = 1
    for idx_k in range(kf):
        if args.loss == 'LLC_loss':
            path_annotation = args.path_annotation+'train_annotation.json'
            j_file = open(path_annotation, "r")
            annotations = json.load(j_file)

            path_annotation1 = args.path_annotation+'test_annotation.json'

            j_file_test = open(path_annotation1, "r")
            test_annotation = json.load(j_file_test)

            # LABELS_URL = 'https://s3.amazonaws.com/outcome-blog/imagenet/labels.json'
            # class_annotation_imagenet = {(value.split(','))[0].replace(' ','_'):int(key)  for (key, value) in requests.get(LABELS_URL).json().items()}

            if j_file:  # and class_annotation_imagenet:
                print("Annotation loaded")
            else:
                print("Error annotation loading.. exit")
                exit()

            dataset = dataset_load.create(name = args.dataset, batch_size = args.batch_size, path = args.path, balanced_classes = args.balanced_classes, train_annotation=annotations,test_annotation=test_annotation,type_loss='LLC')
        elif args.loss =='xent_loss':
            dataset = dataset_load.create(name = args.dataset, batch_size = args.batch_size, path = args.path, balanced_classes = args.balanced_classes, type_loss='normal')

        trainloader, testloader, num_classes = dataset.trainloader, dataset.testloader, dataset.num_classes

        print("Running id: ", idx_k)
        if args.loss == 'LLC_loss':
            file_output = Logger(osp.join(args.save_dir, 'Exp:' + str(idx_k) + '_dat_' + str_dataset + '_gpu= ' + args.gpu  + '_model_' + args.model +'_loss=' + args.loss+'.txt'))
        elif args.loss == 'xent_loss':
            file_output = Logger(osp.join(args.save_dir, 'Exp:' + str(idx_k) + '_dat_' + str_dataset + '_gpu= ' + args.gpu + '_model_' + args.model + '_loss=' + args.loss + '.txt'))

        print("Model {:} Learning rate optimizer {:}  Batch-size {:}  Loss {:}  Graft {:}  Optimizer {:} ".format(args.model,args.lr, args.batch_size,args.loss,args.graft, args.optimizer), file=file_output)
        if base_main.main(args, trainloader, testloader, num_classes, file_output):
            print("Success", file=file_output)
            file_output.close()
        else:
            print("Abort", file=file_output)
            exit()