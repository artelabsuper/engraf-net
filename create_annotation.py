import json
import os

path_train = '/home/supreme/datasets-nas/cifar100/cifar100/train/'
path_annotation_train = '/home/supreme/datasets-nas/cifar100/'


folder_class = []
h_dict_idx = {}
idx = 0

tmp_depth = int(input('Insert depth level for hierachical categories'))

for i in (os.listdir(path_train)):
    if not i.endswith(".tar"):
        folder_class.append(i)
        h_dict_idx[i] = idx
        idx += 1

h_dict = {}


def insert_into_dict(i, n_files):
    for file_name in n_files:
        h_dict.setdefault(file_name, [])
        h_dict[file_name].append(i)


from nltk.corpus import wordnet as wn

all_categories = [[] for i in range(0, tmp_depth)]
all_categoriesidx = [[] for i in range(0, tmp_depth)]
start_level_category=3
end_level_category=start_level_category + tmp_depth
counter_idx = [0 for i in range(0, tmp_depth)]
for idx, i in enumerate(folder_class):
    str_clean = (i.split('_')[0])  # For cifar
    #str_clean=(i.split('__')[0])  # For Imagenet
    try:
        if str_clean=='tiger':
            ss = wn.synsets(str_clean)[1]
        else:
            if str_clean == 'plain':
                str_clean = 'landscape'
            elif str_clean == 'sea':
                str_clean = 'water'
            ss = wn.synsets(str_clean)[0]
    except Exception:
        print(wn.synsets(str_clean))
        exit()

    ss_root = ss.hypernym_paths()

    try:
        syn=[ss_root[0][s].lemma_names('eng')[0] for s in range(0, len(ss_root[0]) - 1)][start_level_category:end_level_category]
        #print(i)
        if len(syn) != tmp_depth:
            print(i)
            print(ss_root)
            syn = [ss_root[0][s].lemma_names('eng')[0] for s in range(0, len(ss_root[0]) - 1)][
                  start_level_category-1:end_level_category]
            print(syn)


        #print(list(reversed([ss_root[0][s].lemma_names('eng')[0] for s in range(0, len(ss_root[0])-1)]))) #All categories except the leaf
    except Exception as e:
        print(e)
        exit()

    if idx > 100:
        exit()
    for idx_s, sy in enumerate(syn):
        if sy not in all_categories[idx_s]:
            all_categories[idx_s].append(sy)
            all_categoriesidx[idx_s].append(counter_idx[idx_s])
            n_files = os.listdir(path_train + '/' + i)
            insert_into_dict(counter_idx[idx_s], n_files)
            counter_idx[idx_s] += 1
        else:
            # get position
            get_idx = all_categories[idx_s].index(sy)
            n_files = os.listdir(path_train + '/' + i)
            insert_into_dict(get_idx, n_files)


# #TODO save
# with open(path_annotation_train+'/cifar100/train_annotation.json', 'w') as fp:
#     json.dump(h_dict, fp)


for j in range(0, tmp_depth):
    print('level ',j, 'Categories:',all_categories[j])
    for i, item in enumerate(all_categories[j]):
        print(i, item)


#Only for test set
path_test = '/home/supreme/datasets-nas/cifar100/cifar100/test/'
path_annotation_test = '/home/supreme/datasets-nas/cifar100/'
folder_class = []
h_dict_idx = {}
idx = 0

for i in (os.listdir(path_test)):
    if not i.endswith(".tar"):
        folder_class.append(i)
        h_dict_idx[i] = idx
        idx += 1




for idx, i in enumerate(folder_class):
    str_clean = (i.split('_')[0])  # For cifar
    # str_clean=(i.split('__')[0])  # For Imagenet
    try:
        if str_clean == 'tiger':
            ss = wn.synsets(str_clean)[1]
        else:
            if str_clean == 'plain':
                str_clean = 'landscape'
            elif str_clean == 'sea':
                str_clean = 'water'
            ss = wn.synsets(str_clean)[0]
    except Exception:
        print(wn.synsets(str_clean))
        exit()
    ss_root = ss.hypernym_paths()

    try:
        syn = [ss_root[0][s].lemma_names('eng')[0] for s in range(0, len(ss_root[0]) - 1)][
              start_level_category:end_level_category]
        # print(i)
        if len(syn) != tmp_depth:
            print(i)
            print(ss_root)
            syn = [ss_root[0][s].lemma_names('eng')[0] for s in range(0, len(ss_root[0]) - 1)][
                  start_level_category-1:end_level_category]
            print(syn)

        # print(list(reversed([ss_root[0][s].lemma_names('eng')[0] for s in range(0, len(ss_root[0])-1)]))) #All categories except the leaf
    except Exception as e:
        print(e)
        exit()
    for idx_s, sy in enumerate(syn):
            # get position
            get_idx = all_categories[idx_s].index(sy)
            n_files = os.listdir(path_test + '/' + i)
            insert_into_dict(get_idx, n_files)







# with open(path_annotation_test+'/cifar100/test_annotation.json', 'w') as fp:
#     json.dump(h_dict, fp)

