# from EnGrafNet_pre import Ext_ResNet
import torch, os, numpy as np

from resnet import resnet50, resnet101, resnet152

os.environ['CUDA_VISIBLE_DEVICES'] = '0,1,2,3'
use_gpu = torch.cuda.is_available()
if use_gpu:
    #str_model_list=['LLC_resnet50','LLC_resnet101', 'LLC_resnet152']
    str_model_list=['resnet50','resnet101', 'resnet152']

    for i in str_model_list:
        # model = resnet50(i, 4, num_classes=100)
        model = resnet152(num_classes=200)
        print(model, np.sum(p.numel() for p in model.parameters() if p.requires_grad))
        model=None
else:
    print("Error")