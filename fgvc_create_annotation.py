import json
import os



path_dataset='/home/super/tmp_datasets/fgvc_dataset/tmp/fgvc-aircraft-2013b/data/'
path_train_hierarchy=path_dataset+'images_family_trainval.txt'
path_families=path_dataset+'families.txt'

#Create id for the class hierarchy "families"
families_dict = {}
f = open(path_families, "r")
for idx, x in enumerate(f):
    family_name=x.replace(' ','_').rstrip("\n")
    families_dict.setdefault(family_name, [])
    families_dict[family_name].append(idx)

folder_class = []
h_dict_idx = {}
idx = 0
#Family 70

h_dict = {}


def insert_into_dict(i, image_name):
    h_dict.setdefault(image_name, [])
    try:
        h_dict[image_name].append(families_dict[i][0])
    except Exception as e:
        print(e)
        exit()

f = open(path_train_hierarchy, "r")
for x in f:
  image_name, family = x.split(' ',1)[0]+'.jpg', x.split(' ', 1)[1].replace(' ','_').rstrip("\n")
  #print(folder_name, genera, families, order)
  #n_files = os.listdir(path_dataset + '/train/' + folder_name)
  insert_into_dict(family, image_name)
f.close()


# # #TODO save
with open(path_dataset+'/train_annotation.json', 'w') as fp:
    json.dump(h_dict, fp)



h_dict = {}
#Only for test set
path_test_hierarchy=path_dataset+'images_family_test.txt'


f = open(path_test_hierarchy, "r")
for x in f:
    image_name, family = x.split(' ', 1)[0] + '.jpg', x.split(' ', 1)[1].replace(' ', '_').rstrip("\n")
    insert_into_dict(family, image_name)
f.close()






with open(path_dataset+'/test_annotation.json', 'w') as fp:
    json.dump(h_dict, fp)

