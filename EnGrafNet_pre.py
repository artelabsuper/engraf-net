'''ResNet in PyTorch.

For Pre-activation ResNet, see 'preact_resnet.py'.

Reference:
[1] Kaiming He, Xiangyu Zhang, Shaoqing Ren, Jian Sun
    Deep Residual Learning for Image Recognition. arXiv:1512.03385
'''
import time

import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.hub import load_state_dict_from_url

__all__ = ['ResNet', 'resnet18', 'resnet34', 'resnet50', 'resnet101',
           'resnet152', 'resnext50_32x4d', 'resnext101_32x8d',
           'wide_resnet50_2', 'wide_resnet101_2']


model_urls = {
    'resnet18': 'https://download.pytorch.org/models/resnet18-5c106cde.pth',
    'resnet34': 'https://download.pytorch.org/models/resnet34-333f7ec4.pth',
    'resnet50': 'https://download.pytorch.org/models/resnet50-19c8e357.pth',
    'resnet101': 'https://download.pytorch.org/models/resnet101-5d3b4d8f.pth',
    'resnet152': 'https://download.pytorch.org/models/resnet152-b121ed2d.pth',
    'resnext50_32x4d': 'https://download.pytorch.org/models/resnext50_32x4d-7cdf4587.pth',
    'resnext101_32x8d': 'https://download.pytorch.org/models/resnext101_32x8d-8ba56ff5.pth',
    'wide_resnet50_2': 'https://download.pytorch.org/models/wide_resnet50_2-95faca4d.pth',
    'wide_resnet101_2': 'https://download.pytorch.org/models/wide_resnet101_2-32ee1156.pth',
}

def conv3x3(in_planes, out_planes, stride=1, groups=1, dilation=1):
    """3x3 convolution with padding"""
    return nn.Conv2d(in_planes, out_planes, kernel_size=3, stride=stride,
                     padding=dilation, groups=groups, bias=False, dilation=dilation)


def conv1x1(in_planes, out_planes, stride=1):
    """1x1 convolution"""
    return nn.Conv2d(in_planes, out_planes, kernel_size=1, stride=stride, bias=False)

#Extended Resnet pretrained
class Ext_ResNet(nn.Module):
    def __init__(self, str_model, graft, num_classes):
        super(Ext_ResNet, self).__init__()
        self.graft=graft
        self.str_model=str_model
        self.num_classes = num_classes
        self.fine_branch = self._model_choice(1000, str_model)
        self.fine_branch.avgpool = nn.AdaptiveAvgPool2d((1, 1))
        self.fine_branch.fc = nn.Linear(512*4, num_classes)

        self.coarse_branch = self._model_choice(1000, str_model)
        self.coarse_branch.avgpool = nn.AdaptiveAvgPool2d((1, 1))
        self.coarse_branch.fc = nn.Linear(512 * 4, 123) #123 for CUB, #71 FGVC dataset

        self.mixed_branch = self._model_choice(1000, str_model)
        self.mixed_branch.avgpool = nn.AdaptiveAvgPool2d((1, 1))
        self.mixed_branch.fc = nn.Linear(512 * 4, num_classes)

        if self.str_model == 'LLC_resnet50' or self.str_model == 'LLC_resnet101' or self.str_model == 'LLC_resnet152':
            # if graft == 2:
            #     self.second_block_feat = nn.Sequential(
            #         nn.Conv2d(64 * block.expansion, 64 * block.expansion, kernel_size=3, padding=1, bias=False),
            #         nn.BatchNorm2d(64 * block.expansion), nn.ReLU(inplace=True),
            #         nn.AdaptiveMaxPool2d((1, 1)),
            #         nn.Flatten())
            #     self.second_block_last = nn.Sequential(
            #         nn.Linear(64 * block.expansion, 20))
            #     self.last_layer_mixed_branch = nn.Linear(512 * block.expansion + 64 * block.expansion, num_classes)
            #     self.final_layer = nn.Linear(512 * 3 * block.expansion + 64 * block.expansion, num_classes)
            #
            #
            # if graft == 3:
            #     self.third_block_feat = nn.Sequential(
            #         nn.Conv2d(128 * block.expansion, 128 * block.expansion, kernel_size=3, padding=1, bias=False),
            #         nn.BatchNorm2d(128 * block.expansion), nn.ReLU(inplace=True),
            #         nn.AdaptiveMaxPool2d((1, 1)),
            #         nn.Flatten())
            #     self.third_block_last = nn.Sequential(
            #         nn.Linear(128 * block.expansion, 20))
            #     self.last_layer_mixed_branch = nn.Linear(512 * block.expansion + 128 * block.expansion, num_classes)
            #     self.final_layer = nn.Linear(512 * 3 * block.expansion + 128 * block.expansion, num_classes)


            if graft == 4:
                self.fourth_block_feat = nn.Sequential(
                    nn.Conv2d(256 * 4, 256 * 4, kernel_size=3, padding=1, bias=False),
                    nn.BatchNorm2d(256 * 4), nn.ReLU(inplace=True),
                    nn.AdaptiveMaxPool2d((1, 1)),
                    nn.Flatten())
                self.fourth_block_last = nn.Sequential(
                    nn.Linear(256 * 4, 123)) #123 for CUB, #123 for CUB, #71 FGVC dataset
                self.last_layer_mixed_branch = nn.Linear(512 * 4 + 256 * 4, num_classes)
                self.final_layer = nn.Linear(512 * 3 * 4 + 256 * 4, num_classes)


            # elif graft == 5:
            #     self.fifth_block_feat = nn.Sequential(
            #         nn.Conv2d(512 * block.expansion, 512 * block.expansion, kernel_size=3, padding=1, bias=False),
            #         nn.BatchNorm2d(512 * block.expansion), nn.ReLU(inplace=True),
            #         nn.AdaptiveMaxPool2d((1, 1)),
            #         nn.Flatten())
            #     self.fifth_block_last = nn.Sequential(
            #         nn.Linear(512 * block.expansion, 20))
            #     self.last_layer_mixed_branch = nn.Linear(512 * block.expansion + 512 * block.expansion, num_classes)
            #     self.final_layer = nn.Linear(512 * 3 * block.expansion + 512 * block.expansion, num_classes)




    def forward(self, x):
        #Fine branch
        out_fine_branch, last_features_fine = self.fine_branch(x, 'base_model', self.graft)
        #Coarse branch
        out_coarse_branch, last_features_coarse = self.coarse_branch(x, 'base_model',self.graft)

        #Mixed branch
        mixed_coarse_feat_raw, x_3 = self.mixed_branch(x, 'graft_model',self.graft)

        if self.graft == 3:
            mixed_coarse_feat = self.third_block_feat(mixed_coarse_feat_raw)  # return the feat of mixed_coarse branch
            mixed_coarse_layer = self.third_block_last(mixed_coarse_feat)  # return the ouput of the graft-coarse mixed branch
        elif self.graft == 4:
            mixed_coarse_feat= self.fourth_block_feat(mixed_coarse_feat_raw)
            mixed_coarse_layer = self.fourth_block_last(mixed_coarse_feat)
        elif self.graft == 5:
            mixed_coarse_feat= self.fifth_block_feat(mixed_coarse_feat_raw)
            mixed_coarse_layer = self.fifth_block_last(mixed_coarse_feat)

        mixed_concat = torch.cat((x_3, mixed_coarse_feat), dim=1)
        out_mixed_branch = self.last_layer_mixed_branch(mixed_concat) #return last output of mixed branch
        final_concat = torch.cat((last_features_fine, last_features_coarse, mixed_concat), dim=1)
        final_output = self.final_layer(final_concat)
        return final_output, out_coarse_branch, out_mixed_branch, mixed_coarse_layer, out_fine_branch

    def _model_choice(self, num_classes, str_model):
        if str_model == 'LLC_resnet152':
            model = ResNet(Bottleneck, [3, 8, 36, 3], num_classes)
            state_dict = load_state_dict_from_url(model_urls['resnet152'])
            model.load_state_dict(state_dict)
            return model
        elif str_model == 'LLC_resnet101':
            model = ResNet(Bottleneck, [3, 4, 23, 3], num_classes)
            state_dict = load_state_dict_from_url(model_urls['resnet101'])
            model.load_state_dict(state_dict)
            return model
        elif str_model == 'LLC_resnet50':
            model = ResNet(Bottleneck, [3, 4, 6, 3], num_classes)
            state_dict = load_state_dict_from_url(model_urls['resnet50'])
            model.load_state_dict(state_dict)
            return model
        elif str_model == 'LLC_resnet18':
            model = ResNet(BasicBlock, [2, 2, 2, 2], num_classes)
            state_dict = load_state_dict_from_url(model_urls['resnet18'])
            model.load_state_dict(state_dict)
            return model



class BasicBlock(nn.Module):
    expansion = 1

    def __init__(self, inplanes, planes, stride=1, downsample=None, groups=1,
                 base_width=64, dilation=1, norm_layer=None):
        super(BasicBlock, self).__init__()
        if norm_layer is None:
            norm_layer = nn.BatchNorm2d
        if groups != 1 or base_width != 64:
            raise ValueError('BasicBlock only supports groups=1 and base_width=64')
        if dilation > 1:
            raise NotImplementedError("Dilation > 1 not supported in BasicBlock")
        # Both self.conv1 and self.downsample layers downsample the input when stride != 1
        self.conv1 = conv3x3(inplanes, planes, stride)
        self.bn1 = norm_layer(planes)
        self.relu = nn.ReLU(inplace=True)
        self.conv2 = conv3x3(planes, planes)
        self.bn2 = norm_layer(planes)
        self.downsample = downsample
        self.stride = stride

    def forward(self, x):
        identity = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)

        if self.downsample is not None:
            identity = self.downsample(x)

        out += identity
        out = self.relu(out)

        return out


class Bottleneck(nn.Module):
    # Bottleneck in torchvision places the stride for downsampling at 3x3 convolution(self.conv2)
    # while original implementation places the stride at the first 1x1 convolution(self.conv1)
    # according to "Deep residual learning for image recognition"https://arxiv.org/abs/1512.03385.
    # This variant is also known as ResNet V1.5 and improves accuracy according to
    # https://ngc.nvidia.com/catalog/model-scripts/nvidia:resnet_50_v1_5_for_pytorch.

    expansion = 4

    def __init__(self, inplanes, planes, stride=1, downsample=None, groups=1,
                 base_width=64, dilation=1, norm_layer=None):
        super(Bottleneck, self).__init__()
        if norm_layer is None:
            norm_layer = nn.BatchNorm2d
        width = int(planes * (base_width / 64.)) * groups
        # Both self.conv2 and self.downsample layers downsample the input when stride != 1
        self.conv1 = conv1x1(inplanes, width)
        self.bn1 = norm_layer(width)
        self.conv2 = conv3x3(width, width, stride, groups, dilation)
        self.bn2 = norm_layer(width)
        self.conv3 = conv1x1(width, planes * self.expansion)
        self.bn3 = norm_layer(planes * self.expansion)
        self.relu = nn.ReLU(inplace=True)
        self.downsample = downsample
        self.stride = stride

    def forward(self, x):
        identity = x

        out = self.conv1(x)
        out = self.bn1(out)
        out = self.relu(out)

        out = self.conv2(out)
        out = self.bn2(out)
        out = self.relu(out)

        out = self.conv3(out)
        out = self.bn3(out)

        if self.downsample is not None:
            identity = self.downsample(x)

        out += identity
        out = self.relu(out)

        return out


class ResNet(nn.Module):

    def __init__(self, block, layers, num_classes, zero_init_residual=False,
                 groups=1, width_per_group=64, replace_stride_with_dilation=None,
                 norm_layer=None):
        super(ResNet, self).__init__()
        if norm_layer is None:
            norm_layer = nn.BatchNorm2d
        self._norm_layer = norm_layer

        self.inplanes = 64
        self.dilation = 1
        if replace_stride_with_dilation is None:
            # each element in the tuple indicates if we should replace
            # the 2x2 stride with a dilated convolution instead
            replace_stride_with_dilation = [False, False, False]
        if len(replace_stride_with_dilation) != 3:
            raise ValueError("replace_stride_with_dilation should be None "
                             "or a 3-element tuple, got {}".format(replace_stride_with_dilation))
        self.groups = groups
        self.base_width = width_per_group
        self.conv1 = nn.Conv2d(3, self.inplanes, kernel_size=7, stride=2, padding=3,
                               bias=False)
        self.bn1 = norm_layer(self.inplanes)
        self.relu = nn.ReLU(inplace=True)
        self.maxpool = nn.MaxPool2d(kernel_size=3, stride=2, padding=1)
        self.layer1 = self._make_layer(block, 64, layers[0])
        self.layer2 = self._make_layer(block, 128, layers[1], stride=2, dilate=replace_stride_with_dilation[0])
        self.layer3 = self._make_layer(block, 256, layers[2], stride=2, dilate=replace_stride_with_dilation[1])
        self.layer4 = self._make_layer(block, 512, layers[3], stride=2, dilate=replace_stride_with_dilation[2])
        self.avgpool = nn.AdaptiveAvgPool2d((1, 1))
        self.fc = nn.Linear(512 * block.expansion, num_classes)

        for m in self.modules():
            if isinstance(m, nn.Conv2d):
                nn.init.kaiming_normal_(m.weight, mode='fan_out', nonlinearity='relu')
            elif isinstance(m, (nn.BatchNorm2d, nn.GroupNorm)):
                nn.init.constant_(m.weight, 1)
                nn.init.constant_(m.bias, 0)

        # Zero-initialize the last BN in each residual branch,
        # so that the residual branch starts with zeros, and each residual block behaves like an identity.
        # This improves the model by 0.2~0.3% according to https://arxiv.org/abs/1706.02677
        if zero_init_residual:
            for m in self.modules():
                if isinstance(m, Bottleneck):
                    nn.init.constant_(m.bn3.weight, 0)
                elif isinstance(m, BasicBlock):
                    nn.init.constant_(m.bn2.weight, 0)

    def _make_layer(self, block, planes, blocks, stride=1, dilate=False):
        norm_layer = self._norm_layer
        downsample = None
        previous_dilation = self.dilation
        if dilate:
            self.dilation *= stride
            stride = 1
        if stride != 1 or self.inplanes != planes * block.expansion:
            downsample = nn.Sequential(
                conv1x1(self.inplanes, planes * block.expansion, stride),
                norm_layer(planes * block.expansion),
            )

        layers = []
        layers.append(block(self.inplanes, planes, stride, downsample, self.groups,
                            self.base_width, previous_dilation, norm_layer))
        self.inplanes = planes * block.expansion
        for _ in range(1, blocks):
            layers.append(block(self.inplanes, planes, groups=self.groups,
                                base_width=self.base_width, dilation=self.dilation,
                                norm_layer=norm_layer))

        return nn.Sequential(*layers)

    def forward(self, data, model_type, graft):
        if model_type =='base_model':
            x = self.conv1(data)
            x = self.bn1(x)
            x = self.relu(x)
            x = self.maxpool(x)
            x = self.layer1(x)
            x = self.layer2(x)
            x = self.layer3(x)
            x = self.layer4(x)
            x = self.avgpool(x)
            last_features = torch.flatten(x, 1)
            x = self.fc(last_features)
            return x, last_features
        elif model_type =='graft_model':
            x_3 = self.relu(self.bn1(self.conv1(data)))
            x_3 = self.maxpool(x_3)
            x_3 = self.layer1(x_3)

            if graft == 2:
                mixed_coarse_feat = x_3

            x_3 = self.layer2(x_3)

            if graft == 3:
                mixed_coarse_feat = x_3

            x_3 = self.layer3(x_3)

            if graft == 4:
                mixed_coarse_feat = x_3


            x_3 = self.layer4(x_3)

            if graft == 5:
                mixed_coarse_feat = x_3

            x_3 = self.avgpool(x_3)
            x_3 = x_3.view(x_3.size(0), -1)
            return mixed_coarse_feat, x_3


