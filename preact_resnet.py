"""preactresnet in pytorch

[1] Kaiming He, Xiangyu Zhang, Shaoqing Ren, Jian Sun

    Identity Mappings in Deep Residual Networks
    https://arxiv.org/abs/1603.05027
"""

import torch
import torch.nn as nn
import torch.nn.functional as F

class PreActBasic(nn.Module):

    expansion = 1
    def __init__(self, in_channels, out_channels, stride):
        super().__init__()
        self.residual = nn.Sequential(
            nn.BatchNorm2d(in_channels),
            nn.ReLU(inplace=True),
            nn.Conv2d(in_channels, out_channels, kernel_size=3, stride=stride, padding=1),
            nn.BatchNorm2d(out_channels),
            nn.ReLU(inplace=True),
            nn.Conv2d(out_channels, out_channels * PreActBasic.expansion, kernel_size=3, padding=1)
        )

        self.shortcut = nn.Sequential()
        if stride != 1 or in_channels != out_channels * PreActBasic.expansion:
            self.shortcut = nn.Conv2d(in_channels, out_channels * PreActBasic.expansion, 1, stride=stride)

    def forward(self, x):

        res = self.residual(x)
        shortcut = self.shortcut(x)

        return res + shortcut


class PreActBottleNeck(nn.Module):

    expansion = 4
    def __init__(self, in_channels, out_channels, stride):
        super().__init__()

        self.residual = nn.Sequential(
            nn.BatchNorm2d(in_channels),
            nn.ReLU(inplace=True),
            nn.Conv2d(in_channels, out_channels, 1, stride=stride),

            nn.BatchNorm2d(out_channels),
            nn.ReLU(inplace=True),
            nn.Conv2d(out_channels, out_channels, 3, padding=1),

            nn.BatchNorm2d(out_channels),
            nn.ReLU(inplace=True),
            nn.Conv2d(out_channels, out_channels * PreActBottleNeck.expansion, 1)
        )

        self.shortcut = nn.Sequential()

        if stride != 1 or in_channels != out_channels * PreActBottleNeck.expansion:
            self.shortcut = nn.Conv2d(in_channels, out_channels * PreActBottleNeck.expansion, 1, stride=stride)

    def forward(self, x):

        res = self.residual(x)
        shortcut = self.shortcut(x)

        return res + shortcut

class PreActResNet(nn.Module):

    def __init__(self, block, num_block, num_classes):
        super().__init__()
        self.input_channels = 64

        self.pre = nn.Sequential(
            nn.Conv2d(3, 64, 3, padding=1),
            nn.BatchNorm2d(64),
            nn.ReLU(inplace=True)
        )

        self.stage1 = self._make_layers(block, num_block[0], 64,  1)
        self.stage2 = self._make_layers(block, num_block[1], 128, 2)
        self.stage3 = self._make_layers(block, num_block[2], 256, 2)
        self.stage4 = self._make_layers(block, num_block[3], 512, 2)

        #NEW
        self.linear1 = nn.Linear(2048, 6)
        self.linear2 = nn.Linear(1024, 10)
        self.linear3 = nn.Linear(512, 17)

        self.first_conv = nn.Sequential(
            nn.BatchNorm2d(64), nn.ReLU(inplace=True),
        nn.Conv2d(64, 128, kernel_size=3, padding=1, bias=False),)

        self.second_conv = nn.Sequential(
            nn.BatchNorm2d(128), nn.ReLU(inplace=True),
        nn.Conv2d(128, 256, kernel_size=3, padding=1, bias=False),
)

        self.third_conv = nn.Sequential(
            nn.BatchNorm2d(256), nn.ReLU(inplace=True),
            nn.Conv2d(256, 512, kernel_size=3, padding=1, bias=False),
        )

        self.fc_modified = nn.Linear(4096, 2048)
        self.last_layer = nn.Linear(2048, num_classes)


    def _make_layers(self, block, block_num, out_channels, stride):
        layers = []

        layers.append(block(self.input_channels, out_channels, stride))
        self.input_channels = out_channels * block.expansion

        while block_num - 1:
            layers.append(block(self.input_channels, out_channels, 1))
            self.input_channels = out_channels * block.expansion
            block_num -= 1

        return nn.Sequential(*layers)

    def forward(self, x):
        x = self.pre(x)

        x = self.stage1(x)
        first_block_conv = self.first_conv(x)
        first_block = F.adaptive_max_pool2d(first_block_conv, (4, 4))
        flatten_x1 = first_block.view(first_block.size(0), -1)
        preds_cluster0 = self.linear1(flatten_x1)

        x = self.stage2(x)
        second_block_conv = self.second_conv(x)

        second_block = F.adaptive_max_pool2d(second_block_conv, (2, 2))
        flatten_x2 = second_block.view(second_block.size(0), -1)
        preds_cluster1 = self.linear2(flatten_x2)

        x = self.stage3(x)
        third_block_conv = self.third_conv(x)
        third_block = F.adaptive_max_pool2d(third_block_conv, (1, 1))
        flatten_x3 = third_block.view(third_block.size(0), -1)
        preds_cluster2 = self.linear3(flatten_x3)

        x = self.stage4(x)
        x = F.adaptive_avg_pool2d(x, 1)
        x = x.view(x.size(0), -1)
        concat = torch.cat((x, flatten_x1, flatten_x2, flatten_x3), dim=1)
        x = F.relu(self.fc_modified(concat))
        x = self.last_layer(x)

        return x, preds_cluster0, preds_cluster1, preds_cluster2


def preactresnet18(num_classes):
    return PreActResNet(PreActBasic, [2, 2, 2, 2],num_classes)

def preactresnet34():
    return PreActResNet(PreActBasic, [3, 4, 6, 3])

def preactresnet50():
    return PreActResNet(PreActBottleNeck, [3, 4, 6, 3])

def preactresnet101(num_classes):
    return PreActResNet(PreActBottleNeck, [3, 4, 23, 3],num_classes)

def preactresnet152(num_classes):
    return PreActResNet(PreActBottleNeck, [3, 8, 36, 3], num_classes)

